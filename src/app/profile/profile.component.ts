import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SearchService } from '../search.service';
import { User } from '../user.model';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  profile: User;
  skills: any[];
  needs: any[];

  constructor(private router: Router, private service: SearchService, private snackBar: MatSnackBar) { 
    this.service.getNeighbor(this.router.getCurrentNavigation().extras.state.id)
      .subscribe((profile: User) => {
        this.profile = profile;
        this.skills = Object.keys(this.profile.skills);
        this.needs = Object.keys(this.profile.needs);
      });
  }

  ngOnInit() {
  }

  showToast(msg: string) {
    this.snackBar.open(msg, 'ok');
  }

}

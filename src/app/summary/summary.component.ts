import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user.model';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {

  @Input() profileSummary: User;
  
  constructor(private router: Router) { }

  ngOnInit() {
    console.log(this.profileSummary);
  }

  viewProfile(profileId) {
    this.router.navigateByUrl('/profile', { state: { id: profileId } });
  }

}

import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { SearchService } from '../search.service';
import { User } from '../user.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  myControl = new FormControl();
  summaryInfo: User[];
  type: string;
  searchStr: string;

  constructor(private service: SearchService) { }

  ngOnInit() {  }

  // search by needs or skills
  search(type: string) {
    this.type = type;
    if(type === 'canHelp') {
      // i can help those who need [skill]
      this.service.getNeeds(this.myControl.value)
      .subscribe(res => {
        this.summaryInfo = res;
      });
    } else if(type === 'needHelp') {
      // i need help from those who have [skill]
      this.service.getSkills(this.myControl.value)
      .subscribe(res => {
        this.summaryInfo = res;
      });
    }
    this.searchStr = this.myControl.value;
  }
}

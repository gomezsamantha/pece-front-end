export interface User {
    id: number;
    username: string;
    name: string;
    email: string;
    city: string;
    state: string;
    friends: string[];
    needs: string[];
    skills: string[];
    stars: number;
    image: string;
    usdRate: string;
    totalNumJobsComplete: number;
    totalNumReviews: number;
    neighborRate: string;
    dateLastJobCompleted: string;
    backgroundCheckStatus: string;
    facebook: string;
    linkedin: string;
    indeed: string;
    thumbtack: string;
    bio: string;
    memberSince: string;
}
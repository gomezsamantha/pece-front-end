import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { User } from './user.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  BASE = 'http://hhe-dev.us-east-2.elasticbeanstalk.com/api/v1/neighbors';

  constructor(private http: HttpClient) { }

  getNeeds(need: string) {
    let params = new HttpParams().set('needs', need);
    return this.http.get(this.BASE, { params })
      .pipe(
        map(res => this.resConverter(res))
      );
  }

  getSkills(skill: string) {
    let params = new HttpParams().set('skills', skill);
    return this.http.get(this.BASE, { params })
      .pipe(
        map(res => this.resConverter(res))
      );
  }

  getNeighbor(id: string) {
    return this.http.get(`${this.BASE}/${id}`)
      .pipe(
        map(res => this.resConverter(res)[0])
      );
  }

  // map response model from api
  private resConverter(res): User[] {
    // make sure we are workig with an array
    if (!Array.isArray(res)) {
      res = [res];
    }
    let convertedRes = [];
    res.forEach(info => {
      convertedRes.push({
        ...info,
        memberSince: info.member_since,
        usdRate: info.usd_rate,
        totalNumJobsComplete: info.total_num_jobs_completed,
        totalNumReviews: info.total_num_reviews,
        neighborRate: info.neighbor_rate,
        dateLastJobCompleted: info.date_last_job_completed,
        backgroundCheckStatus: info.background_check_status,
        friends: JSON.parse(info.friends),
        skills: JSON.parse(info.skills),
        needs: JSON.parse(info.needs),
      });
    });
    return convertedRes;
  }
}
